<?php

namespace App;

use Iodev\Whois\Whois;
use PreviewTechs\cPanelWHM\WHM\Accounts;
use PreviewTechs\cPanelWHM\WHMClient;

class WhmConnect
{
    /* Initial connection to WHM */
    public static function connect()
    {
        return new Accounts(new WHMClient(
            env('WHM_U'),
            env('WHM_TOKEN'),
            env('WHM_SERVER'),
            env('WHM_PORT')
        ));
    }

    public static function whois(){
        return Whois::create();
    }
}